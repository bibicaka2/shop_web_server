<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->post('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

//v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        $api->post('user/register', [
            'uses' => 'UsersInfoController@register'
        ]);
        $api->post('user/update/info', [
            'uses' => 'UsersInfoController@updateUserInfo'
        ]);
        $api->post('user/delete', [
            'uses' => 'UsersInfoController@deleteUserAccount'
        ]);
        $api->post('user/log-in', [
            'uses' => 'UsersController@logIn'
        ]);
        $api->post('user/log-out', [
            'uses' => 'UsersController@logOut'
        ]);
        $api->post('user/validate-log-in', [
            'uses' => 'UsersController@validateLogin'
        ]);
           $api->post('user/find', [
            'uses' => 'UsersInfoController@find'
        ]);
        $api->post('user/search-v1', [
            'uses' => 'UsersInfoController@userListV1'
        ]);
        $api->post('user/all', [
            'uses' => 'UsersController@getUserBasicInfo'
        ]);
        $api->get('user/check-relationship', [
            'uses' => 'UsersInfoController@checkUserRelationship'
        ]); 
         $api->post('user/list', [
            'uses' => 'UsersInfoController@list_all'
        ]);
    });
});
