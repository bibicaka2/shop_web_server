<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
// $app->get('/', function () use ($app) {
//     return $app->version();
// });

$api = app('Dingo\Api\Routing\Router');

// v1 version API
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    $api->group(['middleware' => ['api.locale']], function ($api) {
        //Login
        $api->post('ProductSize/create', [
            'as' => 'productSize.create',
            'uses' => 'ProductSizeController@create',
        ]);

        $api->post('ProductSize/update', [
            'as' => 'productSize.update',
            'uses' => 'ProductSizeController@update',
        ]);

        $api->post('ProductSize/delete', [
            'as' => 'productSize.delete',
            'uses' => 'ProductSizeController@delete',
        ]);

        $api->post('ProductSize/list', [
            'as' => 'productSize.list',
            'uses' => 'ProductSizeController@list',
        ]);
         $api->post('ProductSize/findProductSize', [
            'as' => 'productSize.findProductSize',
            'uses' => 'ProductSizeController@findProductSize',
        ]);

    });
});
