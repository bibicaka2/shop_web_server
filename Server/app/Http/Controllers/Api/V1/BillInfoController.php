<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\ProductsRepository;
use App\Api\Repositories\Contracts\BillInfoRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Department;
use App\Api\Entities\User;
use Illuminate\View\View;
use App\Api\Entities\Products;
use App\Api\Entities\BillInfo;




class BillInfoController extends Controller
{

    protected $userRepository;
    protected $billInfoRepository;
  
    protected $request;

    public function __construct(   
        BillInfoRepository $billInfoRepository, 
         ProductsRepository $productsRepository, 
        Request $request
    ) 
        {
            $this->billInfoRepository = $billInfoRepository;
            $this->productsRepository = $productsRepository;
            $this->request = $request;
            parent::__construct();
        }
    
    public function create()
    {

        $validator = \validator::make($this->request->all(),[
            'name'=>'required',
             'phone'=>'required',
             'email'=>'required',
            'address'=>'required',
            'product_detail'=>'required'
            
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $product_detail_list=$this->request->get('product_detail');
         $name=$this->request->get('name');
          $address=$this->request->get('address');
          $product_size=$this->request->get('product_size');
           $phone=$this->request->get('phone');
             $sale_off_id=$this->request->get('sale_off_id');
             $status="Đang xử lí";
           $email=$this->request->get('email');
        // foreach ($product_detail_list as $key1 => $product_detail) {
        //     $products=[];
        //     $size='';
        //     foreach ($product_detail as $key => $value) {
                
        //         if($key==='product_id')
        //         {
        //             $products = Products::where('_id',mongo_id($value))->first();

        //         }

        //         else if($key==='size_id' || $key==='quantity')      
        //         {   

        //             if($key==='size_id'){
        //                 $size=$value;

        //             }
        //             else{
        //                 for( $i=0; $i <count($products->product_size_list);$i++){
                    
        //                     if($products->product_size_list[$i]['size_id']===$size)
        //                     {

        //                         //cach fix
        //                         //https://laracasts.com/discuss/channels/eloquent/indirect-modification-of-overloaded-element//
        //                         $a=$products->product_size_list;
        //                         (string)$a[$i]['inventory']=(int)$a[$i]['inventory']-(int)($value);
        //                         $products->product_size_list=$a;
        //                         //(String)((int)$products->product_size_list[$i]['inventory']-(int)$value);
        //                         if((int)$products->product_size_list[$i]['inventory']>=0)
        //                         {
        //                             $products->save();
        //                         }
        //                     }
        //                 }
        //                 // foreach ($products->product_size_list as $key1 ) {
        //                 //     $flag=0;
        //                 //     foreach ($key1 as $key2 => $value2) {

        //                 //         if($size===$value2)
        //                 //         {
        //                 //             $flag=1;
        //                 //         } 
        //                 //         else 
        //                 //         {
                                    
        //                 //             if($flag===1)
        //                 //             {
        //                 //                 $value2=$value2-(int)$value;
        //                 //               //  djson($value2);
        //                 //                 if($value2>0)
        //                 //                 {
                                              
        //                 //                         $products->save();
        //                 //                 }    
        //                 //             }
        //                 //         }   
        //                 //     }
                          
        //                 // }
        //             }
        //         }  
        //     }
        // }
     
        $attributes = [       
            'name'=>$name,
            'phone'=>$phone,
            'address'=>$address,
            'sale_off_id'=>$sale_off_id,
            'email'=>$email,
            'status'=>$status,
           'product_detail'=>$this->request->get('product_detail'),       
           
        ];
        //djson($attributes);
        $billInfo =$this->billInfoRepository->create($attributes);
        return $this->successRequest($billInfo);
    }
    
     
    public function confirm()
    {
        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
        $id=$this->request->get('_id');
        $status='Đã xử lí';

        $billInfo = BillInfo::where('_id', mongo_id($id))->first();

        $product_detail_list= $billInfo['product_detail'];
        
        if(!empty($billInfo))
        {    
                  foreach ($product_detail_list as $key1 => $product_detail) {
            $products=[];
            $size='';
            foreach ($product_detail as $key => $value) {
                
                if($key==='product_id')
                {
                    $products = Products::where('_id',mongo_id($value))->first();

                }

                else if($key==='size_id' || $key==='quantity')      
                {   

                    if($key==='size_id'){
                        $size=$value;

                    }
                    else{
                        for( $i=0; $i <count($products->product_size_list);$i++){
                    
                            if($products->product_size_list[$i]['size_id']===$size)
                            {

                                //cach fix
                                //https://laracasts.com/discuss/channels/eloquent/indirect-modification-of-overloaded-element//
                                    $a=$products->product_size_list;
                                    (string)$a[$i]['inventory']=(int)$a[$i]['inventory']-(int)($value);
                                    $products->product_size_list=$a;
                                //(String)((int)$products->product_size_list[$i]['inventory']-(int)$value);
                                if((int)$products->product_size_list[$i]['inventory']>=0)
                                {
                                    $products->save();
                                }
                            }
                        }
                     
                    }
                }  
            }
        }
     
                $billInfo->status= $status;  
                 $billInfo->save();
        }
         //$billInfo->update();
        return $this->successRequest();
    }
    
    public function delete()
    {

        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
        $id=$this->request->get('_id');
        $billInfo=BillInfo::where('_id',mongo_id($id))->first();
        if(!empty($billInfo))
        {   


            $billInfo->delete();
        }
        return $this->successRequest();
    }
    
    public function list()
    {

       $billInfo = BillInfo::all();
       if(count($billInfo)>0)
       {
         foreach ($billInfo as $key => $value) {
             $billInfo_list[]= $value;
                }
                return $billInfo_list;
       }
       return [];
    }

      public function find()
    {

     $validator = \validator::make($this->request->all(),[
            'id'=>'required', 
           
        ]);
        $id = $this->request->get('_id');
       $bill_info_item = BillInfo::where('_id',mongo_id($id))->first();
       if($bill_info_item)
       return $bill_info_item;
        return $this->errorBadRequest('khong co san pham');
    }

}