<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\ProductsRepository;
use App\Api\Repositories\Contracts\ProductSizeRepository;
use App\Api\Repositories\Contracts\ProductStatusRepository;
use App\Api\Repositories\Contracts\SaleOffRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Department;
use App\Api\Entities\User;
use Illuminate\View\View;
use App\Api\Entities\SaleOff;
use App\Api\Entities\ProductStatus;
use App\Api\Entities\ProductSize;
use App\Api\Entities\Products;



class ProductsController extends Controller
{

    protected $userRepository;
    protected $productsRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        ProductsRepository $productsRepository, 
        Request $request
    )
        {
            $this->productsRepository = $productsRepository;
            $this->request = $request;
            parent::__construct();
        }
    
    public function create()
    {
        //rang buoc
        $validator = \validator::make($this->request->all(),[   
            'name'=>'required', 
            'img'=>'required', 
            'status_id'=>'required', 
            'gender'=>'required',
            'description'=>'required',
            'product_size_list'=>'required',
            'rating'=>'required',
            // 'sale_id'=>'required',
            'color'=>'required',
            'product_type_id'=>'required',
            'price_origin'=>'required',
            'price_after_sale_off'=>'required'


        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());       
        }
        
        $name = $this->request->get('name');
        $img =  $this->request->get('img');
        $status_id =  $this->request->get('status_id');
        $description = $this->request->get('description');
        $gender = $this->request->get('gender');
        //$inventory =  $this->request->get('inventory');        
        $rating = $this->request->get('rating');
        // $sale_id =  $this->request->get('sale_id');
        $color = $this->request->get('color');
        $product_type_id =  $this->request->get('product_type_id');
        //$size_id =  $this->request->get('size_id');
        $price_origin =  $this->request->get('price_origin');
        $price_after_sale_off =  $this->request->get('price_after_sale_off');
        $product_size_list= $this->request->get('product_size_list');
      
        // foreach ($product_size_list as $key1 => $size_id) {
        //     $products=[];
        //     foreach ($product_size_list as $key => $inventory) {
        //         if($key==='size_id')
        //         {
        //             $products->size_id =$product_size;
        //         }
        //         else
        //         {
        //             $products->inventory= $inventory;                      
        //             $products->save();                     
        //         } 
        //     }
        // }

        //Khai bao thuoc tinh
        $attributes = [        
            'name'=>$name, 
            'img'=>$img,         
            'status_id'=>$status_id, 
            'description'=>$description,
            'rating'=>$rating,  
            // 'sale_id'=>$sale_id,
            'gender'=>$gender,
            'color'=>$color,
            'product_type_id'=>$product_type_id,
            'product_size_list'=>$product_size_list,
            'price_origin'=>$price_origin,
            'price_after_sale_off'=>$price_after_sale_off
           
        ];
        $products =$this->productsRepository->create($attributes);
        return $this->successRequest($products);
    }
    
    public function update() {
        // Input


       
        $validator = \validator::make($this->request->all(),[
             'name'=>'required', 
            'img'=>'required', 
            'status_id'=>'required', 
            'gender'=>'required',
            'description'=>'required',
            'product_size_list'=>'required',
            'rating'=>'required',
            // 'sale_id'=>'required',
            'color'=>'required',
            'product_type_id'=>'required',
            'price_origin'=>'required',
            'price_after_sale_off'=>'required'
        ]);
        $id = $this->request->get('_id');
        $Products = Products::where([
            '_id'=> mongo_id($id),
        ])->first();
        $name = $this->request->get('name');
        $img =  $this->request->get('img');
        $status_id =  $this->request->get('status_id');
        $description = $this->request->get('description');
        $inventory =  $this->request->get('inventory');       
        $gender =  $this->request->get('gender');    
        $rating = $this->request->get('rating');
        // $sale_id =  $this->request->get('sale_id');
        $product_size_list=$this->request->get('product_size_list');
        $color = $this->request->get('color');
        $product_type_id =  $this->request->get('product_type_id');
        $size_id =  $this->request->get('size_id');
        $price_origin =  $this->request->get('price_origin');
        $price_after_sale_off =  $this->request->get('price_after_sale_off');
        $products = Products::where('_id', mongo_id($id))->first();

        if (!empty($products)) {    
            if(!empty($this->request->get('name')))
            {
                $products->name=$this->request->get('name');
            }
            if(!empty($this->request->get('img')))
            {
                $products->img=$this->request->get('img');
            }
            if(!empty($this->request->get('status_id')))
            {
                $products->status_id=$status_id;
            }
            if(!empty($this->request->get('description')))  
            {
            $products->description=$this->request->get('description');
            }
            if(!empty($this->request->get('gender')))
            {
                $products->gender=$this->request->get('gender');
            }
            // if(!empty($this->request->get('inventory')))
            // {
            //     $products->inventory=$this->request->get('inventory');
            // }
            if(!empty($this->request->get('rating')))
            {
                $products->rating=$this->request->get('rating');
            }
            // if(!empty($this->request->get('sale_id')))
            // {
            //     $products->sale_id=$this->request->get('sale_id');
            // }
             if(!empty($this->request->get('product_size_list')))
            {
                $products->product_size_list=$this->request->get('product_size_list');
            }
            if(!empty($this->request->get('color')))
            {
                $products->color=$this->request->get('color');
            }
            if(!empty($this->request->get('product_type_id')))
            {
                $products->product_type_id=$this->request->get('product_type_id');
            }
          
            if(!empty($this->request->get('price_origin')))
            {
                $products->price_origin=$this->request->get('price_origin');
            }
             if(!empty($this->request->get('price_after_sale_off')))
            {
                $products->price_after_sale_off=$this->request->get('price_after_sale_off');
            }
            
            $products->save();
        }
        return $this->successRequest($products); 
    }
    
    public function delete()
    {

        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
        $id=$this->request->get('_id');
        $products=Products::where('_id',mongo_id($id))->first();
        if(!empty($products))
        {   
            $products->forceDelete();
        }
        return $this->successRequest();
    }
    public function list()
    {

       $products = Products::all();
       if(count($products)>0)
       {
         foreach ($products as $key => $value) {
             $product_list[]= $value;
                }
                return $product_list;
       }
       return [];
    }
    public function findProduct()
    {
        // $products=[];
        // $validator = \validator::make($this->request->all(),[
        //     'key'=>'required',
        // ]);
        // $key=$this->request->get('key');
        // $key=build_slug($key);

        // $params=[
        //     'key'=> $this->request->get('key')
        // ];
        // $product=$this->productsRepository->getProduct($params);
        // return $branch;



        $validator = \validator::make($this->request->all(),[
            'id'=>'required', 
           
        ]);
        $id = $this->request->get('_id');
       $products = Products::where('_id',mongo_id($id))->first();
       if($products)
       return $products;
        return $this->errorBadRequest('khong co san pham');
    }
    public function listProductByType(){
        $products=[];
        $validator =   $validator = \validator::make($this->request->all(),[
            'id'=>'required',   
        ]);
    }
}