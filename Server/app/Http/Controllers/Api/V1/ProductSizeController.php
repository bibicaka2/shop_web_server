<?php
namespace App\Http\Controllers\Api\V1;

use App\Api\Repositories\Contracts\ProductSizeRepository;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthManager;
use Gma\Curl;
use App\Api\Entities\Department;
use App\Api\Entities\User;
use Illuminate\View\View;
use App\Api\Entities\ProductSize;



class ProductSizeController extends Controller
{

    protected $userRepository;
    protected $productSizeRepository;
    protected $auth;
    protected $request;

    public function __construct(   
        ProductSizeRepository $productSizeRepository, 
        Request $request
    )
        {
            $this->productSizeRepository = $productSizeRepository;
            $this->request = $request;
            parent::__construct();
        }
    
    public function create()
    {
        $validator = \validator::make($this->request->all(),[
            'name'=>'required',
        ]);
        if($validator->fails()){
            return $this->errorBadRequest($validator->messages()->toArray());
        }


        
        $name = $this->request->get('name');
       
       
        $attributes = [        
            'name' =>$name,   
           
        ];
        $productSize =$this->productSizeRepository->create($attributes);
        return $this->successRequest($productSize);
    }
    
    public function update() {
        // Input
        $validator = \validator::make($this->request->all(),[
            'name'=>'required', 
          
        ]);
        $id = $this->request->get('_id');
        $productSize = productSize::where('_id', mongo_id($id))->first();
        if (!empty($productSize)) {    
            if(!empty($this->request->get('name')))
            {
                $productSize->name=$this->request->get('name');
            }
         
            $productSize->save();
        }
        return $this->successRequest($productSize); 
    }
    
    public function delete()
    {

        $validator = \validator::make($this->request->all(),[
            '_id'=>'required', 
        ]);
        $id=$this->request->get('_id');
        $productSize=ProductSize::where('_id',mongo_id($id))->first();
        if(!empty($productSize))
        {   
            $productSize->forceDelete();
        }
        return $this->successRequest();
    }
    public function list()
    {

       $productSize = ProductSize::all();
       if(count($productSize)>0)
       {
         foreach ($productSize as $key => $value) {
             $product_list[]= $value;
                }
                return $product_list;
       }
       return [];
    }
    public function findProductSize()
    {
       
        $validator = \validator::make($this->request->all(),[
            'id'=>'required', 
           
        ]);
        $id = $this->request->get('_id');
       $ProductSize = ProductSize::where('_id',mongo_id($id))->first();
       
       return $ProductSize;
    }
    public function listProductByType(){
        $ProductSize=[];
        $validator =   $validator = \validator::make($this->request->all(),[
            'id'=>'required',   
        ]);
    }
}