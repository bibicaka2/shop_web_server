<?php

namespace App\Http\Controllers\Api\V1;

use App\Api\Entities\Status;
use App\Api\Entities\UserFriendRequest;
use App\Api\Entities\Users;
use App\Api\Entities\UsersInfo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Api\Repositories\Contracts\UsersInfoRepository;
use App\Api\Repositories\Contracts\UsersRepository;
use App\Libraries\Gma\APIs\APIAuth;
use App\Libraries\Gma\APIs\APIUpload;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UsersInfoController extends Controller
{
    protected $request;
    protected $userInfoRepository;
    protected $usersRepository;

    public function __construct(Request $request, UsersInfoRepository $userInfoRepository, UsersRepository $usersRepository)
    {
        $this->request = $request;
        $this->userInfoRepository = $userInfoRepository;
        $this->usersRepository = $usersRepository;
    }


    /**
     * api đăng kí người dùng
     * route: [POST]api/user/register
     * request url: http://tlcn-server-test/api/user/register
     * params:
     *      first_name: required
     *      last_name: required,
     *      user_name: required,
     *      password: required
     *      email: required
     *      phone: required|unique:users_info
     *      sex: [
                'required',
                Rule::in([0,1])]
            nation: required
            avatar: nullable|file
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công."
            }
     */
    public function register()
    {
        $validator = \Validator::make($this->request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required',
            'email' => 'required|email|unique:users_info',
            'phone' => 'required|unique:users_info|max:11|min:10',
            'sex' => [
                'required',
                Rule::in([0, 1])
            ],
            'dOb' => 'required|date_format:d/m/Y',
            'avatar' => 'nullable|file'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $first_name = $this->request->get('first_name');
        $last_name = $this->request->get('last_name');
        $user_name = $last_name . ' ' . $first_name;
        $dOb = $this->request->get('dOb');
        $password = $this->request->get('password');
        $email = $this->request->get('email');
        $phone = $this->request->get('phone');
        $sex = $this->request->get('sex');
        $avatar = $this->request->get('avatar');

        $isNumberString = is_number_string($phone);
        if ($isNumberString == -1) {
            return $this->errorBadRequest(trans('core.invalid_phone'));
        }

        //Lưu các thông tin cơ bản của người dùng vào collection userInfo
        $userInfoAttr = [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'phone' => $phone,
            'sex' => $sex,
            'dOb' => $dOb,
            'phone_verified' => '0' //chưa xác thực,
        ];
        $userInfo = UsersInfo::create($userInfoAttr);

        //build no sign profile
        $createDateTime = DateTime::createFromFormat("d/m/Y", $dOb);
        // $date = $createDateTime->format('d');
        $noSignProfile = build_user_name(trim($last_name, '') . '.' . trim($first_name, ' ')) . '.' . $createDateTime->format('d') . '.' . $createDateTime->format('m');
        //check no_sign
        $noSignExisted = Users::where([
            'no_sign_profile' => $noSignProfile
        ])->first();
        if (!empty($noSignExisted)) {
            $randomStr = str_random(4);
            $noSignProfile = $noSignProfile . '.' . $randomStr;
        }

        //Lưu các thông tin khác vào users
        $userAccountAttr = [
            'user_name' => $user_name,
            'no_sign' => build_slug($user_name),
            'no_sign_profile' => $noSignProfile,
            'user_id' => $userInfo->_id,
            'phone' => $phone,
            'email' => $email,
            'password' => $password
        ];
        $userAccount = APIAuth::createUserAccount($userAccountAttr);

        //gửi sms để xác thực số điện thoại
        //tạo OTP random
        $verify_code = mt_rand(10000, 99999);
        $userInfo->verify_code = (string)$verify_code;
        $userInfo->save();

        // APIAuth::smsTwilio($verify_code);
        return $this->successRequest(trans('core.success'));
    }
    /**
     * api đăng kí người dùng
     * route: [POST]api/update/info
     * request url: http://tlcn-server-test/api/user/update/info
     * params:
     *      user_id: required
     *      first_name: nullable
     *      last_name: nullable
     *      user_name: nullable
     *      password: nullable
     *      email: nullable
     *      phone: nullable|unique:users_info
            nation: nullable
            avatar: nullable|file
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công."
            }
     */
    public function find()
    {
        
        $validator = \Validator::make($this->request->all(), [
            '_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
       $user_id= $this->request->get('_id');
        // $user = Auth::user();
        $user = UsersInfo::where('_id', $user_id)->first();
        if (!empty($user)) {
            return $this->successRequest($user);
        }
        return $this->errorBadRequest([]);
    }

    public function updateUserInfo()
    {
        $validator = \Validator::make($this->request->all(), [
            // 'user_id'=>'required',
            // 'email'=>'email|unique:users_info',
            // 'phone'=>'unique:users_info|max:11|min:10',
            'update_type' => [
                'required',
                Rule::in([0, 1, 2, 3, 4, 5,6])
                /**
                 * 0: cập nhật thông tin cá nhân
                 * 1: cập nhoật thông tin đăng nhập và bảo mật (tên hiển thị, mật khẩu đăng nhập)
                 * 2: cập nhật thông tin cài đặt web site theo từng cá nhân
                 * 3: cập nhật thông tin danh sách bạn bè
                 * 4: gửi OTP
                 * 5: xác thực OTP
                 */
            ],
            'password' => 'required_if:' . 'update_type,0'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }

        $update_type = (int)($this->request->get('update_type'));
        //check user info and user login info
        if ($this->request->get('update_type') != 5) {

            $userAccount = Users::where([
                'user_id' => Auth::getPayLoad()->get('user_id')
            ])->first();
            if (empty($userAccount)) {
                return $this->errorBadRequest(trans('user.account_not_exist'));
            }
 
            $userInfo = UsersInfo::where([
                '_id' => mongo_id(Auth::getPayLoad()->get('user_id'))
            ])->first();
            if (empty($userInfo)) {
                return $this->errorBadRequest(trans('user.account_not_exist'));
            }

        }
  
        //Kiểm tra thông tin mật khẩu trước khi tiến hành cập nhật
        $password = $this->request->get('password');
        if (isset($password)) {
            if (!password_verify($password, $userAccount->password)) {
                return $this->errorBadRequest(trans('auth.incorrect_password_confirm'));
            }
        }

        //6 Cập nhật tất cả
            if ($update_type == 6) {

                $validator = \Validator::make($this->request->all(), [
                     'first_name' => 'required',
                    'last_name' => 'required',
                    'password' => 'required',
                    'email' => 'required|email',
                    'phone' => 'required|max:11|min:10',
                    'sex' => [
                        'required',
                        Rule::in([0, 1])
                    ],
                    'dOb' => 'required|date_format:d/m/Y',
                    'avatar' => 'nullable|file'
                ]);
                if ($validator->fails()) {
                    return $this->errorBadRequest($validator->messages()->toArray());
                }

                $email = $this->request->get('email');

                if (isset($email)) {

                    $userInfo->email = $email;
                    $userAccount->email = $email;
                }

                $first_name = $this->request->get('first_name');
                if (isset($first_name) && !empty($first_name)) {
                    $userInfo->first_name = $first_name;
                    $userAccount->user_name = $userInfo->last_name . ' ' . $first_name;
                    $userAccount->no_sign = build_slug($userInfo->last_name . ' ' . $first_name);
                }
                $last_name = $this->request->get('last_name');
                if (isset($last_name) && !empty($last_name)) {
                    $userInfo->last_name = $last_name;
                    $userAccount->user_name = $last_name . ' ' . $userInfo->first_name;
                    $userAccount->no_sign = build_slug($last_name . ' ' . $userInfo->first_name);
                }

                $dOb = $this->request->get('dOb');
                if (isset($dOb)) {
                    $userInfo->dOb = $dOb;
                }
                $phone = $this->request->get('phone');
                if (isset($phone)) {
                    $isNumberString = is_number_string($phone);
                    if ($isNumberString == -1) {
                        return $this->errorBadRequest(trans('core.invalid_phone'));
                    }
                    if (isset($userInfo->phone_verified)) {
                        $userInfo->phone = $phone;
                        $userAccount->phone = $phone;
                    }
                }
            } 
        //0: Cập nhật thông tin cá nhân cở bản (email, first_name, last_name, nation, avatar, cover_picture)
        if ($update_type == 0) {
            $validator = \Validator::make($this->request->all(), [
                'email' => 'email|unique:users_info',
                'phone' => 'unique:users_info|max:11|min:10',
                'dOb' => 'date_format:d/m/Y'
            ]);
            if ($validator->fails()) {
                return $this->errorBadRequest($validator->messages()->toArray());
            }
            $email = $this->request->get('email');
            if (isset($email)) {
                $userInfo->email = $email;
                $userAccount->email = $email;
            }

            $first_name = $this->request->get('first_name');
            if (isset($first_name) && !empty($first_name)) {
                $userInfo->first_name = $first_name;
                $userAccount->user_name = $userInfo->last_name . ' ' . $first_name;
                $userAccount->no_sign = build_slug($userInfo->last_name . ' ' . $first_name);
            }
            $last_name = $this->request->get('last_name');
            if (isset($last_name) && !empty($last_name)) {
                $userInfo->last_name = $last_name;
                $userAccount->user_name = $last_name . ' ' . $userInfo->first_name;
                $userAccount->no_sign = build_slug($last_name . ' ' . $userInfo->first_name);
            }

            $dOb = $this->request->get('dOb');
            if (isset($dOb)) {
                $userInfo->dOb = $dOb;
            }
            $phone = $this->request->get('phone');
            if (isset($phone)) {
                $isNumberString = is_number_string($phone);
                if ($isNumberString == -1) {
                    return $this->errorBadRequest(trans('core.invalid_phone'));
                }
                if (isset($userInfo->phone_verified)) {
                    $userInfo->phone = $phone;
                    $userAccount->phone = $phone;
                }
            }
        } //1: cập nhật các thông tin bảo mật (password)
        // elseif ($update_type == 1) {
        //     $validator = \Validator::make($this->request->all(), [
        //         'old_password' => 'required',
        //         'new_password' => 'required'
        //     ]);
        //     if ($validator->fails()) {
        //         return $this->errorBadRequest($validator->messages()->toArray());
        //     }

        //     $oldPassword = $this->request->get('old_password');
        //     $newPassword = $this->request->get('new_password');

        //     //kiểm tra xem password cũ
        //     if (password_verify($oldPassword, $userAccount->password)) {
        //         $errorData = [];

        //         if ((strlen($newPassword) < 8) || (strlen($newPassword) > 32)) {
        //             if (strlen($newPassword) != 1) {
        //                 array_push($errorData, trans('auth.password_min_8_max_32'));
        //             }
        //         }
        //         if ($newPassword == $oldPassword) {
        //             array_push($errorData, trans('auth.same_old_password'));
        //         }

        //         if (!empty($errorData)) {
        //             return $this->errorBadRequest($errorData);
        //         }
        //         $userAccount->password = password_hash($newPassword, PASSWORD_BCRYPT);
        //     } else {
        //         return $this->errorBadRequest(trans('auth.incorrect_old_password'));
        //     }
        // }

        if (!empty($userInfo)) {
            $userInfo->save();
        }
        if (!empty($userAccount)) {
            $userAccount->save();
        }

        return $this->successRequest(trans('core.success'));
    }
    /**
     * api xoá tài khoản người dùng
     * route: [POST]api/user/delete
     * request url: http://tlcn-server-test/api/user/delete
     * params:
     *      user_id: required
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Thành công."
            }
     */
    public function deleteUserAccount()
    {
        $validator = \Validator::make($this->request->all(), [
            'delete_user_id'=>'required',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        
         $delete_user_id= $this->request->get('delete_user_id');
         $user_id = Auth::getPayLoad()->get('user_id');
       
        if($user_id===$delete_user_id)
        {
              return $this->errorBadRequest('user is using');
        }
        //check userInfo
        $userAccount = Users::where([
            'user_id' => $delete_user_id
        ])->first();

        if (empty($userAccount)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }
        $userInfo = UsersInfo::where([
            '_id' => mongo_id($delete_user_id)
        ])->first();

        if (empty($userInfo)) {
            return $this->errorBadRequest(trans('user.account_not_exist'));
        }
        $userAccount->delete();

  
        $userInfo->delete();
        return $this->successRequest(trans('core.success'));
    }
    /**
     * api tìm kiếm và lấy profile của user
     * route: [GET]api/user/search-v1
     * request url: http://tlcn-server-test/api/user/search-v1
     * params:
     *      type_search: required, Rule::in([0,1])
     *          0: tìm kiếm
     *          1: tìm kiếm xong bấm vào để hiện profile
     *      search_content: string nullable
     *      user_id: required nếu type search = 1
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": [
                    {
                        "user_id": "5f5c5c17ba3700002b00501c",
                        "user_name": "Cơ Khánh",
                        "first_name": "Khánh",
                        "last_name": "Dương Cơ",
                        "phone": "0563283215",
                        "email": "duongcokhanh@gmail.com",
                        "nation": "Việt Nam",
                        "sex": "Nam"
                    }
                ]
            }
     */
    // public function userListV1()
    // {
    //     $validator = \Validator::make($this->request->all(), [
    //         'type_search' => [
    //             'required',
    //             Rule::in([0, 1])
    //             /**
    //              * 0: trả về danh sách các user có tên chứa kí tự cần tìm
    //              * 1: trả về 1 user duy nhất (dùng để xem profile)
    //              */
    //         ]
    //     ]);

    //     if ($validator->fails()) {
    //         return $this->errorBadRequest($validator->messages()->toArray());
    //     }

    //     $type_search = $this->request->get('type_search');
    //     $data = [];

    //     if ($type_search == 0) {
    //         $search_content = $this->request->get('search_content');
    //         $params = [
    //             'phone' => $search_content
    //         ];
    //         $user = $this->userInfoRepository->getUserInfo($params);
    //         //nếu tìm theo số điện thoại không có
    //         if (count($user) == 0) {
    //             $params = [
    //                 'user_name' => build_slug($search_content)
    //             ];
    //             $user = $this->usersRepository->getUsersAccount($params);
    //         }

    //         foreach ($user as $item) {
    //             $data[] = $item->transform();
    //         }
    //     } else {
    //         $validator = \Validator::make($this->request->all(), [
    //             'user_id' => 'required_without:' . 'token',
    //             'token' => 'required_without:' . 'user_id'
    //         ]);

    //         if ($validator->fails()) {
    //             return $this->errorBadRequest($validator->messages()->toArray());
    //         }

    //         $userIdNormal = $this->request->get('user_id');
    //         $user_id = '';
    //         if (isset($userIdNormal)) {
    //             $user_id = $userIdNormal;
    //         } else {
    //             $user_id = Auth::getPayLoad()->get('user_id');
    //         }

    //         $params = [
    //             '_id' => $user_id,
    //             'is_detail' => 1
    //         ];
    //         $user = $this->userInfoRepository->getUserInfo($params);

    //         if (empty($user)) {
    //             return $this->errorBadRequest(trans('users.account_not_exist'));
    //         }
    //         $data = $user->transform();
    //     }

    //     return $this->successRequest($data);
    // }
    /**
     * api kiểm tra quan hệ giữa hai người dùng
     * route: [GET]api/user/check-relationship
     * request url: http://13.67.77.166/api/user/check-relationship
     * params:
     *      friend_id: required
     * headers:
     *      Authorization: bearer + token
        resonse:
            {
                "error_code": 0,
                "message": [
                    "Successfully"
                ],
                "data": "Bạn bè"
            }
     */
    // public function checkUserRelationship()
    // {
    //     $validator = \Validator::make($this->request->all(), [
    //         'friend_id' => 'required'
    //     ]);

    //     if ($validator->fails()) {
    //         return $this->errorBadRequest($validator->messages()->toArray());
    //     }

    //     //check info of friend
    //     $friendId = $this->request->get('friend_id');
    //     $friendInfo = UsersInfo::where([
    //         '_id' => mongo_id($friendId)
    //     ])->first();
    //     if (empty($friendInfo)) {
    //         return $this->errorBadRequest(trans('users.account_not_exist'));
    //     }

    //     //get current user list friend
    //     $currentUser = UsersInfo::where([
    //         '_id' => mongo_id(Auth::getPayLoad()->get('user_id'))
    //     ])->first();
    //     if (empty($currentUser)) {
    //         return $this->errorBadRequest(trans('users.account_not_exist'));
    //     }

    //     /**
    //      * 0: Bạn bè
    //      * 1: Đã gửi yêu cầu kết bạn
    //      * 2: Chờ xác nhận (trường hợp user gửi yêu cầu kết bạn tới currentUser và chờ current xác nhận)
    //      * 3: Không phải bạn bè
    //      */
    //     $currentUserListFriend = $currentUser->list_friends;
    //     if (isset($currentUserListFriend)) {
    //         foreach ($currentUserListFriend as $item) {
    //             if ($friendId == $item) {
    //                 $friendListFriend = $friendInfo->list_friends;
    //                 if (isset($friendListFriend)) {
    //                     foreach ($friendListFriend as $item) {
    //                         if ($currentUser->_id == $item) {
    //                             return $this->successRequest(0);
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }

    //     $currentSent = UserFriendRequest::where([
    //         'sender' => Auth::getPayLoad()->get('user_id'),
    //         'receiver' => $friendId
    //     ])->first();
    //     if (!empty($currentSent)) {
    //         return $this->successRequest(1);
    //     }

    //     $friendSent = UserFriendRequest::where([
    //         'sender' => $friendId,
    //         'receiver' => Auth::getPayLoad()->get('user_id')
    //     ])->first();
    //     if (!empty($friendSent)) {
    //         return $this->successRequest(2);
    //     }

    //     return $this->successRequest(3);
    // }

    public function list_all()
    {
        $validator = \Validator::make($this->request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator->messages()->toArray());
        }
        $users_info = UsersInfo::all();
       if(count($users_info)>0)
       {
         foreach ($users_info as $key => $value) {
             $users_info_list[]= $value;
                }
                return $users_info_list;
       }
       return [];

    }
}
