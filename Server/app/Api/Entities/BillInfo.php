<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\BillInfoTransformer;
use Moloquent\Eloquent\SoftDeletes;

class BillInfo extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'BillInfo';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new BillInfoTransformer();

        return $transformer->transform($this);
    }

}
