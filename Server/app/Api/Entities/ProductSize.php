<?php

namespace App\Api\Entities;

use Moloquent\Eloquent\Model as Moloquent;
use App\Api\Transformers\ProductSizeTransformer;
use Moloquent\Eloquent\SoftDeletes;

class ProductSize extends Moloquent
{
	use SoftDeletes;

	protected $collection = 'product_size';

    protected $guarded = array();

    protected $hidden = ['created_at','updated_at','deleted_at'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transform()
    {
        $transformer = new ProductSizeTransformer();

        return $transformer->transform($this);
    }

}
