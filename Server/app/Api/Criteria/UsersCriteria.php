<?php

namespace App\Api\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;
/**
 * Class UsersCriteria
 */
class UsersCriteria implements CriteriaInterface
{
    protected $params;
    public function __construct($params = [])
    {
        $this->params = $params;
    }
    
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query = $model->newQuery();
        
        if(!empty($this->params['user_name'])){
            $user_name = '%'.(string)($this->params['user_name']).'%';
            $query->where('no_sign','like',$user_name);
        };
        if(!empty($this->params['phone'])){
            $phone = '%'.(string)($this->params['phone']).'%';
            $query->where('phone','like',$phone);
        }
        if(!empty($this->params['email'])){
            $email = '%'.(string)($this->params['email']).'%';
            $query->where('email','like',$email);
        }
        return $query;
    }
}
