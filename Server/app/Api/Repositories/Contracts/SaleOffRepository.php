<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SaleOffRepository
 */
interface SaleOffRepository extends RepositoryInterface
{
    
}
