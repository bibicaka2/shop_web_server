<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductsRepository
 */
interface ProductsRepository extends RepositoryInterface
{
    
}
