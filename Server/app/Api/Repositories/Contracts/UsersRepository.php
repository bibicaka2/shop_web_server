<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 */
interface UsersRepository extends RepositoryInterface
{
    public function getUsersAccount($params = [],$limit = 0);
}
