<?php

namespace App\Api\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BillInfoRepository
 */
interface BillInfoRepository extends RepositoryInterface
{
    
}
