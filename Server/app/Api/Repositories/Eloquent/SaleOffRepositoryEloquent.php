<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\SaleOffRepository;
use App\Api\Entities\SaleOff;
use App\Api\Validators\SaleOffValidator;

/**
 * Class SaleOffRepositoryEloquent
 */
class SaleOffRepositoryEloquent extends BaseRepository implements SaleOffRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SaleOff::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
