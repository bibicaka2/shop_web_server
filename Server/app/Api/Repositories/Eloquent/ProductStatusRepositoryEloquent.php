<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\ProductStatusRepository;
use App\Api\Entities\ProductStatus;
use App\Api\Validators\ProductStatusValidator;

/**
 * Class ProductStatusRepositoryEloquent
 */
class ProductStatusRepositoryEloquent extends BaseRepository implements ProductStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductStatus::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
