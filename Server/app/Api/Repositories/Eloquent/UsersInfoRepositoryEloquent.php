<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\UsersCriteria;
use App\Api\Criteria\UsersInfoCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\usersInfoRepository;
use App\Api\Entities\UsersInfo;
use App\Api\Validators\UsersInfoValidator;

/**
 * Class UsersInfoRepositoryEloquent
 */
class UsersInfoRepositoryEloquent extends BaseRepository implements UsersInfoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UsersInfo::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }

    public function getUserInfo($params = [], $limit = 0){
        $this->pushCriteria(new UsersInfoCriteria($params));
        
        if (!empty($params['is_detail'])) {
            $item = $this->get()->first();
        } elseif (!empty($params['is_paginate'])) {
            if ($limit != 0) {
                $item = $this->paginate($limit);
            } else {
                $item = $this->paginate();
            }
        } else {
            $item = $this->all();
        }
        $this->popCriteria(new UsersInfoCriteria($params));
        return $item;
    }
}
