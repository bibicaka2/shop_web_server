<?php

namespace App\Api\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\BillInfoRepository;
use App\Api\Entities\BillInfo;
use App\Api\Validators\BillInfoValidator;

/**
 * Class BillInfoRepositoryEloquent
 */
class BillInfoRepositoryEloquent extends BaseRepository implements BillInfoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BillInfo::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }
}
