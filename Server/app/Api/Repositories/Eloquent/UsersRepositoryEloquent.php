<?php

namespace App\Api\Repositories\Eloquent;

use App\Api\Criteria\UsersCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Api\Repositories\Contracts\UserRepository;
use App\Api\Repositories\Contracts\UsersRepository;
use App\Api\Entities\Users;
use App\Api\Validators\UsersValidator;

/**
 * Class UsersRepositoryEloquent
 */
class UsersRepositoryEloquent extends BaseRepository implements UsersRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Users::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
    }

    public function getUsersAccount($params = [],$limit = 0){
        $this->pushCriteria(new UsersCriteria($params));

        if(!empty($params['is_detail'])){
            $item = $this->get()->first();
        }elseif(!empty($params['is_paginate'])){
            if($limit != 0){
                $item = $this->paginate($limit);
            }else{
                $item = $this->paginate();
            }
        }else{
            $item = $this->all();
        }

        $this->popCriteria(new UsersCriteria($params));
        return $item;
    }
}
