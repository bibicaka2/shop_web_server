<?php

namespace App\Libraries\Gma\APIs;

use App\Api\Entities\Upload;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class APIUpload
{

    /**
     * Trả về request để gọi API lấy file
     * params truyền vào bao gồm:
     * - type: là hình ảnh hay file thông thường (image|file)
     * - option: lựa chọn lấy file ảnh đại diện, tài liệu hay khác
     * - user_id: mã người dùng
     */
    public static function getFileToClient($params = [])
    {
        $validate = \Validator::make($params, [
            'type' => [
                'required',
                Rule::in(['image', 'file'])
            ],
            'option' => [
                'required',
                Rule::in(['avatars', 'cover_picture', 'status', 'chat'])
            ],
        ]);

        if ($validate->fails()) {
            return $validate->messages()->toArray();
        }

        $type = $params['type'];
        $option = $params['option'];
        $user_id = $params['user_id'];

        if (($option == 'avatars' || $option == 'cover_picture') && (empty($params['is_status']))) {
            $uploaded = Upload::where([
                'type' => 'image',
                'user_id' => $user_id,
                'option' => $option
            ])->orderBy('created_at', 'desc')->first();
            if (!empty($uploaded)) {
                $uri = env('API_IMG') . 'get-image?name=' . $uploaded->file_name . '&option=' . $option;
                return $uri;
            } else {
                $uri = env('API_IMG') . 'get-image?name=' . 'default.jpg' . '&option=' . $option;
                return $uri;
            }
        }

        if (!empty($params['upload_id'])) {
            $uploaded = Upload::where([
                'type' => 'image',
                'user_id' => $user_id,
                'option' => $option,
                '_id' => mongo_id($params['upload_id'])
            ])->first();
            if (!empty($uploaded)) {
                $uri = env('API_IMG') . 'get-image?name=' . $uploaded->file_name . '&option=' . $option;
                return $uri;
            }
        }

        $uploaded = Upload::where([
            'type' => $type,
            'user_id' => $user_id,
        ])->get();
        $data = [];

        if (!empty($uploaded)) {
            foreach ($uploaded as $item) {
                if (strpos($item->path, $option)) {
                    $name = $item->file_name;

                    array_push($data, env('API_IMG') . 'get-image?name=' . $name . '&option=' . $option);
                    // return $data;
                } else {
                    $uri = env('API_IMG') . 'get-image?name=' . 'default.jpg' . '&option=' . $option;
                    array_push($data, $uri);
                }
            }
        }
        return $data;
    }

    /**
     * Insert vào collection Upload và lưu vào folder public
     * params truyền vào:
     * - type: là hình ảnh hay file thông thường (image|file)
     * - option: lựa chọn lấy file ảnh đại diện, tài liệu hay khác
     * - user_id: mã nhân viên
     */
    public static function uploadToServer($params = [], $file)
    {
        $isChanged = 0;
        $user_id = $params['user_id'];

        //Kiểm tra type và option
        $validate = \Validator::make($params, [
            'type' => [
                'required',
                Rule::in(['image', 'files'])
            ],
            'option' => [
                'required',
                Rule::in(['avatars', 'cover_picture', 'status', 'chat'])
            ],
        ]);

        if ($validate->fails()) {
            return trans('core.invalid_format');
        }

        $type = $params['type'];
        $option = $params['option'];
        //Kết thúc kiểm tra type và option

        //Xử lý extension
        $file_extension = strtolower(substr(strrchr($file->getClientOriginalName(), "."), 1));
        /**
         * Lưu file vào path public/...
         */
        $nameRandom = str_random(30);
        $file_name = $nameRandom . '.' . $file_extension;

        //kiểm tra hình ảnh hay file có được đăng bởi user hay chưa.
        //Nếu có rồi thì cập nhật ảnh mới xoá ảnh cũ
        $checkFile = Upload::where([
            'user_id' => $user_id,
            'option' => $option
        ])->first();

        $destination = base_path() . '/public/' . $type . '/' . $option;
        $realPath =  '/public/' . $type . '/' . $option . '/' . $file_name;
        if (!empty($checkFile)) {
            if ($option != 'chat' && $option != 'status') {
                $isChanged = 1;
                //kiểm tra có path trong source
                // $path = '../public/img/avatars/' . $file_name;
                // if (file_exists($destination . '/' . $checkFile->file_name) && ) {
                //     unlink(base_path() . '/public/' . $type . '/' . $option . '/' . $checkFile->file_name);
                // }
            }
        }


        $saveFile = $file->move($destination, $file_name);
        if (isset($params['test'])) {
            djson('djjj');
        }

        /**
         * Kết thúc lưu file vào path public/...
         */

        /**
         * lưu file vào database
         */

        //update avatar
        if ($isChanged == 1) {
            // $checkFile->file_name = $file_name;
            // $checkFile->path = $realPath;
            // $checkFile->extension = $file_extension;
            // $checkFile->save();
            $uploadParams = [
                'path' => $realPath,
                'user_id' => $user_id,
                'type' => $type,
                'option' => $option,
                'file_name' => $file_name,
                'extension' => $file_extension
            ];


            $createUpload = Upload::create($uploadParams);
            return $createUpload->_id;
        } else {
            $uploadParams = [
                'path' => $realPath,
                'user_id' => $user_id,
                'type' => $type,
                'option' => $option,
                'file_name' => $file_name,
                'extension' => $file_extension
            ];

            $createUpload = Upload::create($uploadParams);
            return $createUpload->_id;
        }
        return;
    }
}
